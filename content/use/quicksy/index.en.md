---
title: Quicksy
icon: icon.png
replaces:
    - whatsapp
    - facebook-messenger
---

**Quicksy** is an easy-to-use chat app. You can register with just your phone number.

It’s based on the XMPP standard, so if you know someone with an XMPP account you can communicate with them as well.

{{< infobox >}}
- **Android app:**
    - [Quicksy](https://play.google.com/store/apps/details?id=im.quicksy.client) ([FDroid](https://f-droid.org/de/packages/im.quicksy.client/))
{{< /infobox >}}