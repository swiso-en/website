---
title: Dat Project
icon: icon.png
lists: 
    - wip
---

**The Dat project** is creating a new set of technical standards called “Dat”, which provide a privacy-conscious alternative to HTTP. 

The "**H**yper**T**ext **T**ransfer **P**rotocol" currently is the foundation of the world wide web and powers most websites, like this one. Unfortunately, HTTP rules are being exploited to invade people’s privacy.

Dat is being developed by a mixture of non-profit organisations, community groups and volunteers, and it aims to give users more control over who sees their data.

{{< infobox >}}
- **Website:**
    - [DatProject.org](https://datproject.org/)
{{< /infobox >}}
