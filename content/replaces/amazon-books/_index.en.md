---
title: Amazon Books
subtitle: Book Stores
provider: amazon
order: 
    - local-bookshops
    - public-libraries
    - better-world-books
    - hive
    - kobo
    - libro-fm
    - public-domain-ebooks
    - open-library
    - libreture
    - inventaire
aliases:
    - /ethical-alternatives-to-amazon-and-goodreads/
---

Amazon is very convenient to use, but the convenience comes at a price, both for the [zero hours workers in their warehouses][workers] and for the communities that [Amazon deprives of taxes][taxes]. Amazon also has a [history of tracking][trackers] its users.

The eBooks sold by Amazon through its Kindle service have a severe restriction: [Digital Rights Management][drm] (DRM). DRM means books are locked to a particular device, and Amazon can edit or even remove your Kindle eBooks at any time, even after you have bought them. [Amazon infamously did this][orwell] to George Orwell’s “1984” and “Animal Farm”. One Kindle user had her entire library [deliberately wiped by Amazon][wipe] - and they refused to tell her why. In effect, you never really own a Kindle book, it belongs to Amazon forever.

Goodreads and Audible are owned by Amazon, and so contribute to these problems. Luckily there are lots of more ethical alternatives, online and offline, whether you want physical books, eBooks, audiobooks or eReaders:

[drm]: {{< relref "/articles/digital-rights-management" >}}
[orwell]: https://www.nytimes.com/2009/07/18/technology/companies/18amazon.html
[taxes]: https://www.theguardian.com/technology/2015/jun/24/amazons-uk-business-paid-119m-tax-last-year
[trackers]: https://www.telegraph.co.uk/technology/amazon/8797591/Amazons-Kindle-Fire-will-track-users-across-the-web.html
[wipe]: https://www.wired.com/2012/10/amazons-remote-wipe-of-customers-kindle-highlights-perils-of-drm/
[workers]: https://www.channel4.com/news/anger-at-amazon-working-conditions