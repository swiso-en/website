---
title: Thanks
url: "/thanks"
---
<center>

**We say thanks** <br>
to each and every one <br>
who participated in<br>
creating this.

**Be it directly** <br>
via opening issues, <br>
discussing of content, <br>
drafting of texts and <br>
writing of code.

**Be it indirectly** <br>
via asking questions, <br>
searching for answers, <br>
sharing the results and <br>
spreading the word.

**Be it essentially** <br>
by contributing to any<br>
free / libre / open projects <br>
of any shape, size or form <br>
for a public benefit.

**This is yours.** <br>
A tribute to a space of <br>
hope, trust, and freedom <br>
with all its imperfections <br>
and inspirations.

**We say thanks** <br>
to you, and especially <br>
to the anonymous author <br>
who made this small website <br>
a part of the digital journey <br>
we keep on enjoying<br>
**Together.**

</center>

[{{< img src="switching-social.jpg" alt="edited screenshot of switching.social" >}}][switching.social]

[switching.social]: https://web.archive.org/web/20190915101437/https://switching.social/